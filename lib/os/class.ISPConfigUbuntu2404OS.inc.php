<?php
/**
 * Description of class
 *
 * @author croydon
 */
class ISPConfigUbuntu2404OS extends ISPConfigUbuntu2204OS {

	protected function configureApt() {
		// enable contrib and non-free
		ISPConfigLog::info('Configuring apt repositories.', true);

		if($this->getOSArchitecture() == 'arm64') {
			$contents = '# updated by ISPConfig auto installer
Types: deb
URIs: http://ports.ubuntu.com/ubuntu-ports/
Suites: noble noble-updates noble-backports
Components: main restricted universe multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg

Types: deb
URIs: http://ports.ubuntu.com/ubuntu-ports/
Suites: noble-security
Components: main restricted universe multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg
';
		 } else {
			$contents = '# updated by ISPConfig auto installer
Types: deb
URIs: http://archive.ubuntu.com/ubuntu/
Suites: noble noble-updates noble-backports
Components: main restricted universe multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg

Types: deb
URIs: http://security.ubuntu.com/ubuntu/
Suites: noble-security
Components: main restricted universe multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg
';
		 }
		file_put_contents('/etc/apt/sources.list.d/ubuntu.sources', $contents);
	}

	protected function getPackagesToInstall($section) {
		$packages = parent::getPackagesToInstall($section);

		if($section === 'first') {
			$packages[] = 'rsyslog';
		}

		return $packages;
	}

	protected function getSystemPHPVersion() {
		return '8.3';
	}

	protected function addRspamdRepo() {
		ISPConfigLog::info("Not activating Rspamd repository as it's not available for Ubuntu 24.04.", true);
	}

	protected function installRoundcube($mysql_root_pw) {
		parent::installRoundcube($mysql_root_pw);
		$this->replaceLine('/etc/roundcube/config.inc.php', "\$config['smtp_host']", "\$config['smtp_host'] = 'localhost:25';");
	}

}
